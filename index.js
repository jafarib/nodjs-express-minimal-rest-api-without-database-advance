const startupDebugger = require("debug")("app:startup");
const dbDebugger = require("debug")("app:db");
const config = require("config");
const morgan = require("morgan");
const helmet = require("helmet");

const logger = require("./middleware/logger");
const courses = require('./routes/courses')
const home = require('./routes/home')
const express = require("express");
const app = express();

app.set('view engine', 'pug');
app.set('views', './views');

// process.env.NODE_ENV // undifined
console.log(`NODE_ENV:  ${process.env.NODE_ENV}`);
console.log(`app: ${app.get("env")}`);

app.use(express.json()); // req.body
app.use(express.urlencoded({ extended: true })); // key=vale&key=value
app.use(express.static("public")); // serv static content
app.use(helmet());
app.use('/api/courses', courses);
app.use('/', home);

// Configuration
console.log(`Aapplicaction Name: ${config.get("name")}`);
console.log(`Mail Server : ${config.get("mail.host")}`);
console.log(`Password Mail Server : ${config.get("mail.password")}`);

// set NODE_ENV=development
// set NODE_ENV=production
if (app.get("env") === "development") {
  app.use(morgan("tiny"));
  startupDebugger("Morgan enabled");
}

//Db work... 
dbDebugger("Database Bugs");

app.use(logger);
 
// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listen to port ${port}....`));
